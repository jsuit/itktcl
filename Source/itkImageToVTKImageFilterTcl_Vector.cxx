#include <tcl.h>

int ITKTCL_ImageToVTKImageFilter_Vector_Init( Tcl_Interp *interp );

#define INPUT_TYPE Vector3DImageType
#define TCL_COMMAND_NAME ImageToVTKImageFilter_Vector
#include "itkImageToVTKImageFilterTcl.txx"
