#include "itkUtilsTcl.h"
#include "itkImageTypes.h"
#include "itkImageTcl.h"
#include "itkCompose3DVectorImageFilter.h"

typedef itk::Compose3DVectorImageFilter< Scalar3DImageType, Vector3DImageType > ComposeFilterType;

int ComposeFilterCmd( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  // this is how we get the pointer to the itk::Object
  itk::LightObject *itkObj = static_cast< itk::LightObject* >( clientData );
  ComposeFilterType::Pointer composer = dynamic_cast< ComposeFilterType* >( itkObj );
  if ( !composer ) {
    Tcl_AppendResult( interp, "command ", Tcl_GetString( objv[ 0 ] ), " does not references a valid component compose filter", NULL );
    return TCL_ERROR;
  }
  if ( objc < 2 ) {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "ComposeFilter ERROR\n",
                      "wrong # of arguments, must be:\n",
                      "    ", cmd, " SetInput1 image3d\n",
		      "    ", cmd, " SetInput2 image3d\n",
		      "    ", cmd, " SetInput3 image3d\n",
                      "    ", cmd, " GetOutput\n",
                      "    ", cmd, " Update\n",
                      NULL );
    return TCL_ERROR;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Set Input1
  ////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "SetInput1" ) ) 
  {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    if ( objc != 3) {
      Tcl_AppendResult( interp,  "wrong # of arguments, must be:\n",
                                 "    ", cmd, "SetInput1 Image3D", NULL );
      return TCL_ERROR;
    }
    const char* img_name = Tcl_GetString( objv[ 2 ] );
    Scalar3DImageType *ptrImg =
      ITKTCL_GetImageFromCommand<Scalar3DImageType>( interp, img_name );
    if ( !ptrImg ) {
      Tcl_AppendResult( interp, img_name, " id not a valid ImageType",  NULL );
      return TCL_ERROR;
    }
    composer->SetInput1( ptrImg );
    return TCL_OK;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Set Input2
  ////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "SetInput2" ) ) 
  {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    if ( objc != 3) {
      Tcl_AppendResult( interp,  "wrong # of arguments, must be:\n",
                                 "    ", cmd, "SetInput1 Image3D", NULL );
      return TCL_ERROR;
    }
    const char* img_name = Tcl_GetString( objv[ 2 ] );
    Scalar3DImageType *ptrImg =
      ITKTCL_GetImageFromCommand<Scalar3DImageType>( interp, img_name );
    if ( !ptrImg ) {
      Tcl_AppendResult( interp, img_name, " id not a valid ImageType",  NULL );
      return TCL_ERROR;
    }
    composer->SetInput2( ptrImg );
    return TCL_OK;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Set Input3
  ////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "SetInput3" ) ) 
  {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    if ( objc != 3) {
      Tcl_AppendResult( interp,  "wrong # of arguments, must be:\n",
                                 "    ", cmd, "SetInput1 Image3D", NULL );
      return TCL_ERROR;
    }
    const char* img_name = Tcl_GetString( objv[ 2 ] );
    Scalar3DImageType *ptrImg =
      ITKTCL_GetImageFromCommand<Scalar3DImageType>( interp, img_name );
    if ( !ptrImg ) {
      Tcl_AppendResult( interp, img_name, " id not a valid ImageType",  NULL );
      return TCL_ERROR;
    }
    composer->SetInput3( ptrImg );
    return TCL_OK;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Get Output
  ////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetOutput" ) ) 
  {
    return ITKTCL_GetOutput<Vector3DImageType>( interp,  composer->GetOutput( ) );
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Update
  ////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "Update" ) ) 
  {
    try {
      composer->Update( );
    } catch ( itk::ExceptionObject e ) {
      Tcl_AppendResult( interp, e.GetDescription(), NULL );
      return TCL_ERROR;
    }
    return TCL_OK;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  // Else Error Message
  ////////////////////////////////////////////////////////////////////////////////////////////
  else
  {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "ComposeFilter ERROR\n",
                      "wrong subcommand ",
                      Tcl_GetString( objv[ 1 ] ),
                      " must be:\n",
                      "    ", cmd, " SetInput1 image3d\n",
                      "    ", cmd, " SetInput2 image3d\n",
                      "    ", cmd, " SetInput3 image3d\n",
		      "    ", cmd, " GetOutput\n",
                      "    ", cmd, " Update\n",
                      NULL );
      return TCL_ERROR;
  }
}

int CreateComposeFilter( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  // Create Extract Component Filter
  ComposeFilterType::Pointer composer = ComposeFilterType::New();
  return ITKTCL_CreateObjectCommand( interp, ComposeFilterCmd, composer );
}

int ITKTCL_ComposeFilter_Init( Tcl_Interp *interp )
{
  Tcl_CreateObjCommand( interp, "ComposeFilter", CreateComposeFilter, NULL, NULL );
  return TCL_OK;
}
