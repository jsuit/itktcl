#include "itkImageTcl.h"
#include "itkUtilsTcl.h"
#include "itkImageTypes.h"


int Tcl_SetPixelValueResult( Tcl_Interp *interp, PixelType pixel )
{
  Tcl_Obj *result = Tcl_NewDoubleObj( static_cast<double>( pixel ) );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelValueResult( Tcl_Interp *interp, const VPixelType & pixel )
{
  Tcl_Obj **vector;
  int size = pixel.GetSize( );
  
  vector = new Tcl_Obj*[ size ];
  for( int i = 0; i < size; ++i ) {
    vector[ i ] = Tcl_NewDoubleObj( static_cast<double>( pixel[i] ) );
  }
  Tcl_Obj *result =  Tcl_NewListObj( size, vector );
  delete []vector;
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelValueResult( Tcl_Interp *interp, 
                             const ConvariantVector3 & pixel )
{
  Tcl_Obj **vector;
  int size = ConvariantVector3::GetCovariantVectorDimension();
  
  vector = new Tcl_Obj*[ size ];
  for( int i = 0; i < size; ++i ) {
    vector[ i ] = Tcl_NewDoubleObj( static_cast<double>( pixel[i] ) );
  }
  Tcl_Obj *result =  Tcl_NewListObj( size, vector );
  delete []vector;
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelValueResult( Tcl_Interp *interp, const VPixel3Type & pixel )
{
  Tcl_Obj *vector[ 3 ];
  
  for( int i = 0; i < 3; ++i ) {
    vector[ i ] = Tcl_NewDoubleObj( static_cast<double>( pixel[i] ) );
  }
  Tcl_Obj *result =  Tcl_NewListObj( 3, vector );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelValueResult( Tcl_Interp *interp, MaskPixelType pixel )
{
  Tcl_Obj *result = Tcl_NewIntObj( static_cast<int>( pixel ) );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelValueResult( Tcl_Interp *interp, int pixel )
{
  Tcl_Obj *result = Tcl_NewIntObj( pixel );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelSizeResult( Tcl_Interp *interp, PixelType pixel)
{
  Tcl_Obj * result = Tcl_NewIntObj( 1 );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelSizeResult( Tcl_Interp *interp, const VPixel3Type & pixel )
{
  Tcl_Obj * result = Tcl_NewIntObj( 3 );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_SetPixelSizeResult( Tcl_Interp *interp, const ConvariantVector3 & pixel )
{
  Tcl_Obj * result = Tcl_NewIntObj( ConvariantVector3::GetCovariantVectorDimension() );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

int Tcl_GetPixelValueFromObj( Tcl_Interp *interp, Tcl_Obj * obj,
                              PixelType &pixelValue )
{
  double doubleValue;
  if ( Tcl_GetDoubleFromObj(interp, obj, &doubleValue) != TCL_OK ) {
    // error converting from string to double
    return TCL_ERROR;
  }
  pixelValue = static_cast<PixelType>( doubleValue );
  return TCL_OK;
  
}

int Tcl_GetPixelValueFromObj( Tcl_Interp *interp, Tcl_Obj * obj,
                              MaskPixelType &pixelValue )
{
  int intValue;
  if ( Tcl_GetIntFromObj(interp, obj, &intValue) != TCL_OK ) {
    // error converting from string to integer
    return TCL_ERROR;
  }
  pixelValue = static_cast<MaskPixelType>( intValue );
  return TCL_OK;
}

int ITKTCL_Image_Init( Tcl_Interp *interp )
{
  return TCL_OK;
}
