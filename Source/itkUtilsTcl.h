#ifndef _ITK_UTILS_TCL_H_
#define _ITK_UTILS_TCL_H_

#include <tcl.h>
#include "itkLightObject.h"

#define ITKTCL_TO_STRING0( x ) #x
#define ITKTCL_TO_STRING(  x )  ITKTCL_TO_STRING0( x )

#define ITKTCL_PASTE2( a, b) a##b
#define ITKTCL_PASTE( a, b) ITKTCL_PASTE2( a, b)

#if ( defined( _WIN32 ) || defined( WIN32 ) )
#define ITKTCL_EXPORT __declspec(dllexport)
#else
/* unix needs nothing */
#define ITKTCL_EXPORT
#endif

int ITKTCL_Utils_Init( Tcl_Interp *interp );

int ITKTCL_CreateObjectCommand( Tcl_Interp       *interp,
                                Tcl_ObjCmdProc   *cmdProc,
                                itk::LightObject *itkObj );

int ITKTCL_GetCommand( Tcl_Interp       *interp,
                       Tcl_ObjCmdProc   *cmdProc,
                       itk::LightObject *itkObj );

itk::LightObject*
ITKTCL_GetLightObjectFromCommandName( Tcl_Interp *interp,
                                      const char *cmdName );

template <class TObject>
TObject *ITKTCL_GetObjectFromCommandName( Tcl_Interp *interp,
                                          const char *cmdName )
{
  itk::LightObject *itkObj =
    ITKTCL_GetLightObjectFromCommandName( interp, cmdName );
  return dynamic_cast< TObject* >( itkObj );
};

template <class TObject>
TObject *ITKTCL_GetObjectFromClientData( void * internalClientData )
{
  itk::LightObject *itkObj = static_cast< itk::LightObject* >( internalClientData );
  return dynamic_cast< TObject* >( itkObj );
}

int ITKTCL_GetNaturalNumber( Tcl_Interp *interp,
                             Tcl_Obj* obj, int* ptrValue );

int ITKTCL_GetPositiveIntegerNumber( Tcl_Interp *interp,
                                     Tcl_Obj* obj, int* ptrValue );

int ITKTCL_GetDouble( Tcl_Interp *interp,
                      Tcl_Obj* obj, double* ptrValue );

#endif
