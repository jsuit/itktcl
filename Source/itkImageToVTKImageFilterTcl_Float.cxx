#include <tcl.h>

int ITKTCL_ImageToVTKImageFilter_Float_Init( Tcl_Interp *interp );

#define INPUT_TYPE Scalar3DImageType
#define TCL_COMMAND_NAME ImageToVTKImageFilter_Float
#include "itkImageToVTKImageFilterTcl.txx"
