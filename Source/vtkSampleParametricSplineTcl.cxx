#include "vtkSampleParametricSplineTcl.h"
#include "vtkParametricSpline.h"
#include "vtkTclUtil.h"

int SampleParametricSpline_Proc(ClientData clientData, 
                                Tcl_Interp *interp,
                                int objc, Tcl_Obj *const objv[] )
{
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 3)
    {
    Tcl_AppendResult(interp,  "SampleParametricSpline ERROR --\n",
                     "wrong # of arguments, must be:\n",
                     "    ", cmd, " Spline N", NULL);
    return TCL_ERROR;
    }
  int error = 0;
  const char* argSpline = Tcl_GetString(objv[1]);
  vtkParametricSpline *spline = static_cast<vtkParametricSpline *>(
    vtkTclGetPointerFromObject(argSpline,"vtkParametricSpline",
                               interp, error));
  if (error || !spline)
    {
    Tcl_AppendResult(interp,  "SampleParametricSpline ERROR --\n",
                     "could not convert '", argSpline, 
                     "' to a vtkParametricSpline", NULL);
    return TCL_ERROR;
    }
  int status;
  int nPoints;
  status = Tcl_GetIntFromObj(interp, objv[2], &nPoints);
  if (status != TCL_OK)
    return status;
  if (nPoints < 2)
    {
    Tcl_AppendResult(interp, "SampleParametricSpline ERROR --\n",
                     "invalid number of sample points sould be >=2", NULL);
    return TCL_ERROR;
    }
  Tcl_Obj **objvPoints = new Tcl_Obj* [nPoints];
  
  float delta = 1.0/(nPoints-1);
  double u[3], Pt[3], Du[3];
  Tcl_Obj* objPt[3];
  for (int i = 0; i < nPoints; i++)
    {
    u[0] = i*delta;
    spline->Evaluate(u, Pt, Du);
    for(int j = 0; j < 3; j++)
      {
      objPt[j] = Tcl_NewDoubleObj(Pt[j]);
      }
    objvPoints[i] = Tcl_NewListObj(3, objPt);
    }

  Tcl_SetObjResult(interp, Tcl_NewListObj(nPoints, objvPoints));
  delete []objvPoints;
  
  return TCL_OK;
}

int ITKTCL_SampleParametricSplineTcl_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "SampleParametricSpline",
                       SampleParametricSpline_Proc,
                       NULL, NULL);
  return TCL_OK;
  
}
