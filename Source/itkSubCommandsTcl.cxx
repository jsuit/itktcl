#include "itkReaderEvents.h"
#include "itkCommand.h"
#include "itkSubCommandsTcl.h"


template <class TObjectType>
TObjectType * ITKTCL_GetITKObject( Tcl_Interp *interp, char* objCmd )
{

  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, objCmd );
  TObjectType * p = dynamic_cast<TObjectType*>( ptrObj );
  if ( !p ) {
    Tcl_AppendResult( interp, objCmd, " is not a valid given type",  NULL );
    return NULL;
  }
  return p;
}

int ITKTCL_tryGetProgress( itk::ProcessObject *itkObj,
                           const char* strCaller,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "GetProgress" ) )
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp, strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " GetProgress",
                      NULL );
    return TCL_ERROR;
  }
  Tcl_SetObjResult( interp, Tcl_NewDoubleObj( itkObj->GetProgress() ) );
  return TCL_OK;
}

class ItkTclCallback
{
private:
  Tcl_Interp *interp;
  Tcl_Obj *scriptObj;

public:

  ItkTclCallback( Tcl_Interp *_interp, Tcl_Obj* _scriptObj )
  {
    interp = _interp;
    scriptObj = _scriptObj;
    Tcl_IncrRefCount( scriptObj );
  };

  ~ItkTclCallback( )
  {
    if ( scriptObj )
      Tcl_DecrRefCount( scriptObj );
  }

  static void Execute( itk::Object *o, const itk::EventObject &e,
                       void* ClientData )
  {
    ItkTclCallback *_this = static_cast<ItkTclCallback *>(ClientData);
    Tcl_EvalObjEx( _this->interp, _this->scriptObj, TCL_EVAL_GLOBAL );
  }
  
  static void ConstExecute( const itk::Object *o, const itk::EventObject &e,
                            void* ClientData )
  {
    ItkTclCallback *_this = static_cast<ItkTclCallback *>(ClientData);
    Tcl_EvalObjEx( _this->interp, _this->scriptObj, TCL_EVAL_GLOBAL );
  }
  
  static void Delete( void* ClientData )
  {
    ItkTclCallback *_this = static_cast<ItkTclCallback *>(ClientData);
    delete _this;
  }
};

int ITKTCL_tryAddObserver( itk::Object * itkObj,
                           const char* strCaller,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "AddObserver" ) )
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 4 ) {
    Tcl_AppendResult( interp, strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                        "    ", cmd, " AddObserver EventName Script",
                      NULL );
    return TCL_ERROR;
  }
  const char* eventName = Tcl_GetString( objv[ 2 ] );
  Tcl_Obj *scriptObj = objv[ 3 ];
  itk::CStyleCommand::Pointer itkCommand = itk::CStyleCommand::New();
  /* StartEvent */
  if ( !strcmp( "StartEvent",  eventName ) ) {
    itkObj->AddObserver( itk::StartEvent(), itkCommand );
  }
  /* EndEvent */
  else if ( !strcmp( "EndEvent",  eventName ) ) {
    itkObj->AddObserver( itk::EndEvent(), itkCommand );
  }
  /* ProgressEvent */
  else if ( !strcmp( "ProgressEvent",  eventName ) ) {
    itkObj->AddObserver( itk::ProgressEvent(), itkCommand );
  }
  /* StartScanUIDEvent */
  else if ( !strcmp( "StartScanUIDEvent",  eventName ) ) {
    itkObj->AddObserver( itk::StartScanUIDEvent(), itkCommand );
  }
  /* ProgressUIDEvent */
  else if ( !strcmp( "ProgressUIDEvent",  eventName ) ) {
    itkObj->AddObserver( itk::ProgressUIDEvent(), itkCommand );
  }
  /* EndScanUIDEvent */
  else if ( !strcmp( "EndScanUIDEvent",  eventName ) ) {
    itkObj->AddObserver( itk::EndScanUIDEvent(), itkCommand );
  }
  /* StartReadEvent */
  else if ( !strcmp( "StartReadEvent",  eventName ) ) {
    itkObj->AddObserver( itk::StartReadEvent(), itkCommand );
  }
  /* ProgressReadEvent */
  else if ( !strcmp( "ProgressReadEvent",  eventName ) ) {
    itkObj->AddObserver( itk::ProgressReadEvent(), itkCommand );
  }
  /* EndReadEvent */
  else if ( !strcmp( "EndReadEvent",  eventName ) ) {
    itkObj->AddObserver( itk::EndReadEvent(), itkCommand );
  }
  /* unrecognized event */
  else {
    Tcl_AppendResult( interp, strCaller,
                      " ERROR in AddObserver -- unrecognized event '",
                      eventName,
                      "' must be: StartEvent, EndEvent, ProgressEvent",
                      NULL );
    return TCL_ERROR;
  }
  ItkTclCallback *callBack = new ItkTclCallback( interp, scriptObj );
  itkCommand->SetClientData( callBack );
  itkCommand->SetCallback( ItkTclCallback::Execute );
  itkCommand->SetConstCallback( ItkTclCallback::ConstExecute );
  itkCommand->SetClientDataDeleteCallback( ItkTclCallback::Delete );
  return TCL_OK;
}

int ITKTCL_tryUpdate( itk::ProcessObject * process,
                      const char* strCaller,
                      Tcl_Interp *interp,
                      int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "Update" ) )
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp, strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " Update",
                      NULL );
    return TCL_ERROR;
  }
  try {
    process->Update( );
  } catch ( itk::ExceptionObject e ) {
    Tcl_AppendResult( interp, strCaller,
                      " exception : ",
                      e.GetDescription(), NULL );
    return TCL_ERROR;
  }
  return TCL_OK;
}
