#ifndef _itkSubCommandsTcl_
#define _itkSubCommandsTcl_

/* mode: -*- c++ -*- */
#include "itkImageTcl.h"
#include "itkProcessObject.h"
#include "itkImageSource.h"

#if defined(_MSC_VER)
#define snprintf _snprintf
#endif

int ITKTCL_tryGetProgress( itk::ProcessObject *itkObj,
                           const char* strCaller,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[] );

int ITKTCL_tryAddObserver( itk::Object *itkObj,
                           const char* strCaller,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[] );

template <class TObjectType>
TObjectType * ITKTCL_GetITKObject(Tcl_Interp *interp, char* objCmd);

template <class FilterType>
int ITKTCL_trySetInputFrom(FilterType* filter,
                           const char* strCaller,
                           const char* strImageType,
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[])
{
  typedef typename FilterType::InputImageType ImageType;
  typedef typename itk::ImageSource<ImageType> InputFilterType;

  if (strcmp(Tcl_GetString(objv[1]), "SetInputFrom"))
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 3)
    {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " SetInputFrom Filter|Image", NULL );
    return TCL_ERROR;
    }
  const char* cmdSource = Tcl_GetString(objv[2]);
  itk::LightObject *ptrObj =
    ITKTCL_GetLightObjectFromCommandName(interp, cmdSource);
  InputFilterType *tryProcess = dynamic_cast<InputFilterType*>(ptrObj);
  if (tryProcess)
    {
      // is an ImageSource
      filter->SetInput(tryProcess->GetOutput());
      return TCL_OK;
    }
  else
    {
    // check if it is the expected ImageType
    ImageType *ptrImage = dynamic_cast<ImageType*>(ptrObj);
    if (ptrImage)
      {
      filter->SetInput(ptrImage);
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult( interp,  strCaller,
                        " ERROR -- wrong input image type from '",
                        cmdSource, "', must be '", strImageType,
                        "' or a filter returning that image type",
                        NULL);
      return TCL_ERROR;
      }
    }
}

template <class FilterType>
int ITKTCL_trySetNthInputFrom(FilterType* filter,
                              int idx,
                              const char* strCaller,
                              const char* strImageType,
                              Tcl_Interp *interp,
                              int objc, Tcl_Obj *const objv[])
{
  typedef typename FilterType::InputImageType ImageType;
  typedef typename itk::ImageSource<ImageType> InputFilterType;
  char buffer[256];

  snprintf(buffer, 255, "SetInput%dFrom", idx);
  const char* cmd = Tcl_GetString(objv[0]);
  if (idx < 1)
    {
    Tcl_AppendResult(interp,  strCaller,
                     " Error -- Invalid index in SetNthInputFrom, must be positive:\n",
                     "    ", cmd, " ", buffer, NULL);
    return TCL_ERROR;
    }
  if (strcmp(Tcl_GetString(objv[1]), buffer))
    return TCL_CONTINUE;
  if (objc != 3)
    {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", buffer, " Filter|Image", NULL );
    return TCL_ERROR;
    }
  const char* cmdSource = Tcl_GetString(objv[2]);
  itk::LightObject *ptrObj =
    ITKTCL_GetLightObjectFromCommandName(interp, cmdSource);
  InputFilterType *tryProcess = dynamic_cast<InputFilterType*>(ptrObj);
  unsigned int _idx = (unsigned int)(idx-1);
  if (tryProcess)
    {
    // is an ImageSource
    filter->SetInput(_idx, tryProcess->GetOutput());
    return TCL_OK;
    }
  else
    {
    // check if it is the expected ImageType
    ImageType *ptrImage = dynamic_cast<ImageType*>(ptrObj);
    if (ptrImage)
      {
      filter->SetInput(_idx, ptrImage);
      return TCL_OK;
      }
    else
      {
      Tcl_AppendResult( interp,  strCaller,
                        " ERROR -- wrong input image type from '",
                        cmdSource, "', must be '", strImageType,
                        "' or a filter returning that image type",
                        NULL);
      return TCL_ERROR;
      }
    }
}

template <class ImageType>
int ITKTCL_trySetInput(const char* checkName,
                       const char* strCaller,
                       const char* strImageType,
                       Tcl_Interp* interp,
                       int objc, Tcl_Obj* const objv[],
                       ImageType* &ptrImage)
{
  if (strcmp(Tcl_GetString(objv[1]), checkName))
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString(objv[0]);
  if (objc != 3)
    {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", checkName, " ", strImageType, NULL );
    return TCL_ERROR;
    }
  const char* imgCmdName = Tcl_GetString(objv[2]);
  itk::LightObject *ptrObj =
    ITKTCL_GetLightObjectFromCommandName(interp, imgCmdName);
  ptrImage = dynamic_cast<ImageType*>( ptrObj );
  if ( !ptrImage )
    {
    Tcl_AppendResult( interp, strCaller, " ERROR -- " ,
                      imgCmdName,
                      " is not a valid ", strImageType, " image type", NULL );
    return TCL_ERROR;
    }
  return TCL_OK;
}

template <class FilterType>
int ITKTCL_trySetInput( FilterType* filter,
                        const char* strCaller,
                        const char* strImageType,
                        Tcl_Interp *interp,
                        int objc, Tcl_Obj *const objv[] )
{
  typedef typename FilterType::InputImageType ImageType;
  
  ImageType* ptrImage;
  int check = ITKTCL_trySetInput("SetInput", strCaller, strImageType,
                                 interp, objc, objv, ptrImage);
  if (check!=TCL_OK)
    return check;

   filter->SetInput( ptrImage );
   return TCL_OK;
}

template <class FilterType>
int ITKTCL_trySetInput1( FilterType* filter,
                         const char* strCaller,
                         const char* strImageType,
                         Tcl_Interp *interp,
                         int objc, Tcl_Obj *const objv[] )
{
  typedef typename FilterType::InputImageType ImageType;
  
  ImageType* ptrImage;
  int check = ITKTCL_trySetInput("SetInput1", strCaller, strImageType,
                                 interp, objc, objv, ptrImage);
  if (check!=TCL_OK)
    return check;
  
   filter->SetInput1(ptrImage);
   return TCL_OK;
}

template <class FilterType>
int ITKTCL_trySetInput2( FilterType* filter,
                         const char* strCaller,
                         const char* strImageType,
                         Tcl_Interp *interp,
                         int objc, Tcl_Obj *const objv[] )
{
  typedef typename FilterType::InputImageType ImageType;

  ImageType* ptrImage;
  int check = ITKTCL_trySetInput("SetInput2", strCaller, strImageType,
                                 interp, objc, objv, ptrImage);
  if (check!=TCL_OK)
    return check;
  
   filter->SetInput2(ptrImage);
   return TCL_OK;  
}

template <class FilterType>
int ITKTCL_trySetInput3( FilterType* filter,
                         const char* strCaller,
                         const char* strImageType,
                         Tcl_Interp *interp,
                         int objc, Tcl_Obj *const objv[] )
{
  typedef typename FilterType::InputImageType ImageType;

  ImageType* ptrImage;
  int check = ITKTCL_trySetInput("SetInput3", strCaller, strImageType,
                                 interp, objc, objv, ptrImage);
  if (check!=TCL_OK)
    return check;
  
   filter->SetInput3(ptrImage);
   return TCL_OK;  
}

template <class FilterType>
int ITKTCL_trySetBoolProperty( FilterType* filter,
                               const char* strCaller,
                               const char* SubCommand,
                               void (FilterType::*ptrSet)( bool ),
                               Tcl_Interp *interp,
                               int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, " 0|1", NULL );
    return TCL_ERROR;
  }
  int intValue;
  if ( Tcl_GetIntFromObj(interp, objv[ 2 ], &intValue) != TCL_OK ) {
    // error converting from string to integer
    return TCL_ERROR;
  }
  if ( intValue != 0 && intValue != 1 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong boolean value '",
                      Tcl_GetString( objv[ 2 ] ), "'", NULL );    
    return TCL_ERROR;
  }
  (filter->*ptrSet)( static_cast<bool>(intValue) );
  return TCL_OK;
}

template <class FilterType>
int ITKTCL_trySetPropertyOn( FilterType * filter,
                             const char* strCaller,
                             const char* SubCommand,
                             void (FilterType::*ptrSetOn)(),
                             Tcl_Interp *interp,
                             int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  (filter->*ptrSetOn)();
  return TCL_OK;
}

template <class FilterType>
int  ITKTCL_trySetPropertyOff( FilterType * filter,
                               const char* strCaller,
                               const char* SubCommand,
                               void (FilterType::*ptrSetOff)(),
                               Tcl_Interp *interp,
                               int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  (filter->*ptrSetOff)();
  return TCL_OK;
}

template <class FilterType>
int  ITKTCL_tryVoidMethod( FilterType * filter,
                           const char* strCaller,
                           const char* SubCommand,
                           void (FilterType::*ptrMethod)(),
                           Tcl_Interp *interp,
                           int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  (filter->*ptrMethod)();
  return TCL_OK;
}

template <class FilterType, class RealType>
int ITKTCL_trySetRealProperty( FilterType * filter,
                               const char* strCaller,
                               const char* SubCommand,
                               void (FilterType::*ptrSet)( RealType ),
                               Tcl_Interp *interp,
                               int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, " realValue", NULL );
    return TCL_ERROR;
  }
  double doubleValue;
  if ( Tcl_GetDoubleFromObj(interp, objv[ 2 ], &doubleValue) != TCL_OK ) {
    // error converting from string to double
    return TCL_ERROR;
  }
  (filter->*ptrSet)( doubleValue );
  return TCL_OK;
}

template <class FilterType, class RealType>
int ITKTCL_trySetNotNegRealProperty( FilterType * filter,
                                     const char* strCaller,
                                     const char* SubCommand,
                                     void (FilterType::*ptrSet)( RealType ),
                                     Tcl_Interp *interp,
                                     int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, " realValue", NULL );
    return TCL_ERROR;
  }
  double doubleValue;
  if ( Tcl_GetDoubleFromObj(interp, objv[ 2 ], &doubleValue) != TCL_OK ) {
    // error converting from string to double
    return TCL_ERROR;
  }
  if ( doubleValue < 0 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong not negative value '",
                      Tcl_GetString( objv[ 2 ] ), "'", NULL );    
    return TCL_ERROR;
  }
  (filter->*ptrSet)( doubleValue );
  return TCL_OK;
}

template <class FilterType, class RealType>
int ITKTCL_tryGetRealProperty( FilterType * filter,
                               const char* strCaller,
                               const char* SubCommand,
                               RealType (FilterType::*ptrGet)() const,
                               Tcl_Interp *interp,
                               int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  Tcl_SetObjResult( interp, Tcl_NewDoubleObj( (filter->*ptrGet)() ) );
  return TCL_OK;
}

template <class FilterType, class RealType>
int ITKTCL_tryGetRealConstRefProperty( FilterType * filter,
                               const char* strCaller,
                               const char* SubCommand,
                               const RealType &(FilterType::*ptrGet)() const,
                               Tcl_Interp *interp,
                               int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  Tcl_SetObjResult( interp, Tcl_NewDoubleObj( (filter->*ptrGet)() ) );
  return TCL_OK;
}

template <class FilterType, class IntType>
int ITKTCL_trySetNotNegIntProperty( FilterType * filter,
                                    const char* strCaller,
                                    const char* SubCommand,
                                    void (FilterType::*ptrSet)( IntType ),
                                    Tcl_Interp *interp,
                                    int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, " NotNegIntValue", NULL );
    return TCL_ERROR;
  }
  int intValue;
  if ( Tcl_GetIntFromObj(interp, objv[ 2 ], &intValue) != TCL_OK ) {
    // error converting from string to double
    return TCL_ERROR;
  }
  if ( intValue < 0 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong not negative integer value '",
                      Tcl_GetString( objv[ 2 ] ), "'", NULL );    
    return TCL_ERROR;
  }
  (filter->*ptrSet)( intValue );
  return TCL_OK;
}

template <class FilterType, class IntType>
int ITKTCL_tryGetIntProperty( FilterType * filter,
                              const char* strCaller,
                              const char* SubCommand,
                              IntType (FilterType::*ptrGet)() const,
                              Tcl_Interp *interp,
                              int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  Tcl_SetObjResult( interp, Tcl_NewIntObj( (filter->*ptrGet)() ) );
  return TCL_OK;
}

template <class FilterType, class IntType>
int ITKTCL_tryGetIntConstRefProperty( FilterType * filter,
                                      const char* strCaller,
                                      const char* SubCommand,
                                      const IntType& (FilterType::*ptrGet)() const,
                                      Tcl_Interp *interp,
                                      int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), SubCommand ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", SubCommand, NULL );
    return TCL_ERROR;
  }
  Tcl_SetObjResult( interp, Tcl_NewIntObj( (filter->*ptrGet)() ) );
  return TCL_OK;
}

template <class FilterType>
int ITKTCL_trySetRadiusProperty( FilterType * filter,
                                 const char* strCaller,
                                 Tcl_Interp *interp,
                                 int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "SetRadius" ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", " SetRadius NotNegIntValue", NULL );
    return TCL_ERROR;
  }
  int intValue;
  if ( Tcl_GetIntFromObj(interp, objv[ 2 ], &intValue) != TCL_OK ) {
    // error converting from string to int
    return TCL_ERROR;
  }
  if ( intValue < 0 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong not negative integer value '",
                      Tcl_GetString( objv[ 2 ] ), "'", NULL );    
    return TCL_ERROR;
  }
  typename FilterType::InputImageType::SizeType radius;
  radius[0] = intValue;
  radius[1] = intValue;
  radius[2] = intValue;

  filter->SetRadius( radius );
  return TCL_OK;
}

template <class FilterType>
int ITKTCL_tryGetRadiusProperty( FilterType * filter,
                                 const char* strCaller,
                                 Tcl_Interp *interp,
                                 int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "GetRadius" ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " GetRadius", NULL );
    return TCL_ERROR;
  }
  const typename FilterType::InputImageType::SizeType&
    radius = filter->GetRadius();
  Tcl_Obj * result = Tcl_NewIntObj( radius[0] );
  Tcl_SetObjResult( interp, result );
  return TCL_OK;
}

template <class FilterType >
int ITKTCL_trySetPixelValueProperty( FilterType * filter,
                                     const char* strCaller,
                                     const char* strSetProperty,
                                     void (FilterType::*ptrSet)( const typename FilterType::OutputImagePixelType),
                                     Tcl_Interp *interp,
                                     int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), strSetProperty ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", strSetProperty, " PixelValue", NULL );
    return TCL_ERROR;
  }
  typename FilterType::OutputImagePixelType pixelValue;
  if ( Tcl_GetPixelValueFromObj( interp, objv[ 2 ], pixelValue ) == TCL_ERROR ) {
    return TCL_ERROR;
  }
  (filter->*ptrSet)( pixelValue );
  return TCL_OK;
}

template <class FilterType >
int ITKTCL_tryGetPixelValueProperty( FilterType * filter,
                                 const char* strCaller,
                                 const char* strGetProperty,
                                 typename FilterType::OutputImagePixelType (FilterType::*ptrGet)() const,
                                 Tcl_Interp *interp,
                                 int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), strGetProperty ) )
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", strGetProperty, NULL );
    return TCL_ERROR;
  }
  typename FilterType::OutputImagePixelType pixelValue = (filter->*ptrGet)();
  return Tcl_SetPixelValueResult( interp, pixelValue );
}

template <class FilterType >
int ITKTCL_trySetVectorSizeProperty( FilterType * filter,
                                     const char* strCaller,
                                     const char* strSetProperty,
                                     void (FilterType::*ptrSet)( const typename FilterType::InputImageSizeType::SizeValueType data[]),

                                    Tcl_Interp *interp,
                                    int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), strSetProperty ) )
    return TCL_CONTINUE;
  
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 3) {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " ", strSetProperty, " VectorSize", NULL );
    return TCL_ERROR;
  }
  Tcl_Obj **theList;
  int length;
  if ( Tcl_ListObjGetElements(interp, objv[ 2 ],
                              &length, &theList ) != TCL_OK ) {
    return TCL_ERROR;
  }
  unsigned int idim = FilterType::ImageDimension;
  typename FilterType::InputImageSizeType padSize;
  
  if ( (unsigned)length == 1 || (unsigned)length == idim ) {
    int intValue;
    unsigned int i;
    for ( i = 0; i < idim; i++ ) {
      if ( Tcl_GetIntFromObj(interp, theList[ i ], &intValue) != TCL_OK ) {
        // error converting from string to integer
        return TCL_ERROR;
      }
      if ( intValue < 0 ) {
        Tcl_AppendResult( interp,  strCaller,
                          " ERROR -- in ", strSetProperty,
                           "wrong size value, must >= 0, '",
                          Tcl_GetString( theList[ i ] ), "'", NULL );    
        return TCL_ERROR;
      }
      padSize[ i ] = (unsigned int)( intValue );
    }
    for ( ; i < idim; i++ ) {
      padSize[ i ] = (unsigned int)( intValue );
    }
  } else {
    Tcl_AppendResult( interp,  strCaller,
                      " ERROR -- wrong vector size in ",
                      strSetProperty, NULL );
    return TCL_ERROR;
  }
  (filter->*ptrSet)( padSize.m_Size );
  return TCL_OK;
}

template <class OutputImageType>
int ITKTCL_tryGetOutput( itk::ImageSource<OutputImageType> * source,
                         const char* strCaller,
                         Tcl_Interp *interp,
                         int objc, Tcl_Obj *const objv[] )
{
  if ( strcmp( Tcl_GetString( objv[ 1 ] ), "GetOutput" ) )
    return TCL_CONTINUE;
  const char* cmd = Tcl_GetString( objv[ 0 ] );
  if ( objc != 2 ) {
    Tcl_AppendResult( interp, strCaller,
                      " ERROR -- wrong # of arguments, must be:\n",
                      "    ", cmd, " GetOutput",
                      NULL );
    return TCL_ERROR;
  }
  return ITKTCL_GetCommand( interp, ImageCommand<OutputImageType>, source->GetOutput() ); 
}

int ITKTCL_tryUpdate( itk::ProcessObject *process,
                      const char* strCaller,
                      Tcl_Interp *interp,
                      int objc, Tcl_Obj *const objv[] );

#endif
